use super::common::Attachment;
use crate::bindings::gl;
use crate::bindings::gl::types::GLenum;

pub fn attachment_gl_type(att: &Attachment) -> GLenum {
    match att {
        Color0 => gl::COLOR_ATTACHMENT0,
        Color1 => gl::COLOR_ATTACHMENT1,
        Color2 => gl::COLOR_ATTACHMENT2,
        Color3 => gl::COLOR_ATTACHMENT3,
        Color4 => gl::COLOR_ATTACHMENT4,
        Color5 => gl::COLOR_ATTACHMENT5,
        Color6 => gl::COLOR_ATTACHMENT6,
        Color7 => gl::COLOR_ATTACHMENT7,
        Depth => gl::DEPTH_ATTACHMENT,
        DepthStencil => gl::DEPTH_STENCIL_ATTACHMENT,
        Stencil => gl::STENCIL_ATTACHMENT,
    }
}
