#[cfg(target_os="linux")]
pub type EGLNativeDisplayType = *mut std::ffi::c_void;
#[cfg(target_os="linux")]
pub type EGLNativePixmapType = *mut u32;
#[cfg(target_os="linux")]
pub type EGLNativeWindowType = *mut u32;
