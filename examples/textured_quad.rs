extern crate pge;

use log::error;
use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    platform::unix::WindowExtUnix,
    window::WindowBuilder,
};

use std::io::Read;

use pge::gfx;
use pge::gfx::common::*;

fn main() {
    env_logger::init();

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    let disp = match window.xlib_display() {
        Some(d) => d,
        None => {
            error!("Failed to get xlib display");
            return;
        }
    };
    let win = match window.xlib_window() {
        Some(w) => w,
        None => {
            error!("failed to get xlib window");
            return;
        }
    };
    let platform = Platform::Xlib(disp as _, win as _);

    let mut gfx = gfx::Gfx::new(platform).unwrap();
    //assert!(gfx.initialize(platform));

    let mut vs_data = Vec::new();
    let vs;
    if std::fs::File::open("examples/resources/shaders/textured_quad.vert")
        .expect("failed to open file")
        .read_to_end(&mut vs_data)
        .is_ok()
    {
        vs = gfx.create_vertex_shader(&vs_data);
    } else {
        error!("failed to read vertex shader file");
        return;
    }

    let mut fs_data = Vec::new();
    let fs;
    if std::fs::File::open("examples/resources/shaders/textured_quad.frag")
        .expect("failed to open file")
        .read_to_end(&mut fs_data)
        .is_ok()
    {
        fs = gfx.create_fragment_shader(&fs_data);
    } else {
        error!("failed to read fragment shader file");
        return;
    }

    let vb_data = [
        -0.5f32, -0.5f32, 0f32, 0f32, 0f32, 0.5f32, -0.5f32, 0f32, 1f32, 0f32, 0.5f32, 0.5f32,
        0f32, 1f32, 1f32, -0.5f32, 0.5f32, 0f32, 0f32, 1f32,
    ];
    let attributes = [
        Attribute {
            location: 0,
            type_: AttributeType::F32,
            num: 3,
            normalized: false,
        },
        Attribute {
            location: 1,
            type_: AttributeType::F32,
            num: 2,
            normalized: false,
        },
    ];
    let (_, vb_data_u8_slice, _) = unsafe { &vb_data[..].align_to::<u8>() };
    let vb = gfx.create_vertex_buffer(vb_data_u8_slice, &attributes);

    let ib_data = [0u32, 1u32, 2u32, 2u32, 3u32, 0u32];
    let (_, ib_data_u8_slice, _) = unsafe { &ib_data[..].align_to::<u8>() };
    let ib = gfx.create_index_buffer(ib_data_u8_slice, IndexType::U32);

    let tex_data = [
        255u8, 0u8, 0u8, 255u8, 0u8, 255u8, 0u8, 255u8, 0u8, 0u8, 255u8, 255u8, 255u8, 255u8,
        255u8, 255u8,
    ];
    let tex = gfx.create_texture_2d(2, 2, Format::RGBA8, 1);
    gfx.update_texture(tex, 0, 0, 0, 0, 2, 2, 0, &tex_data);
    let sampler = gfx.get_sampler(
        Filter::Nearest,
        Filter::Nearest,
        Wrap::Repeat,
        Wrap::Repeat,
        Wrap::Repeat,
    );

    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::EventsCleared => {
                // Application update code.

                // Queue a RedrawRequested event.
                window.request_redraw();
            }
            Event::WindowEvent {
                event: WindowEvent::RedrawRequested,
                ..
            } => {
                // Redraw the application.
                //
                // It's preferrable to render in this event rather than in EventsCleared, since
                // rendering in here allows the program to gracefully handle redraws requested
                // by the OS.
                gfx.clear_render_target(DEFAULT_RENDER_TARGET);
                gfx.set_vertex_buffer(vb);
                gfx.set_index_buffer(ib);
                gfx.set_vertex_shader(vs);
                gfx.set_fragment_shader(fs);
                gfx.set_texture(tex, sampler, 0);
                gfx.draw(1, 0);

                gfx.present(PresentMode::VSync);
            }
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                let handle = RenderTargetHandle::default();
                println!("handle is_valid: {}", handle.is_valid());
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            // ControlFlow::Poll continuously runs the event loop, even if the OS hasn't
            // dispatched any events. This is ideal for games and similar applications.
            _ => *control_flow = ControlFlow::Poll,
            // ControlFlow::Wait pauses the event loop if no events are available to process.
            // This is ideal for non-game applications that only update in response to user
            // input, and uses significantly less power/CPU time than ControlFlow::Poll.
            // _ => *control_flow = ControlFlow::Wait,
        }
    });
}
